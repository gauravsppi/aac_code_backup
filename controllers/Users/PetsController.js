const Pets=require("../../models/Users/Pets.model");
const formidable = require('formidable');
const Validation = require("../../config/validations.js");
const Validate = Validation.addPetValidations
const fs = require('fs');
const path = require('path');


// Add new pet
exports.addPet = (req, res) => {
    var response={};
    var form = new formidable.IncomingForm();
    form.multiples=true;
    form.uploadDir = path.join('/var/www/animalalert.com/html/nodeapis/uploads/PetsImg/');
    form.parse(req, function(err, fields, files) {
        var dataFields = fields;
        var fileName = '';
        const {error}=Validate(dataFields);
        if(error){
            response.code=400;
            response.message=error.details[0].message;
            return res.send(response);
        }else{
            if (files.images.length=='undefined') {
            var imagesArr=[];
                for(i=1; i <= files.images.length; i++){
                    var file_name = files.images[i-1].name;
                    fileName = Date.now() + '.' + file_name;
                    if (err) {
                        response.code=400;
                        response.message=err.message;
                        return res.send(response);
                    }else{
                        imagesArr.push(fileName); 
                        if(i==files.images.length){
                            response.code=200;
                            response.message='Images uploaded';
                            response.images=imagesArr;
                            return res.send(response);
                        }
                    }
                }
            }else{
                response.code=400;
                response.message='Images not found for upload. Please select images first';
                return res.send(response);
            }
        }
    })
};