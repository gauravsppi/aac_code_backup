const User = require("../../models/Users/User.model");
const bcrypt = require('bcrypt');
const Validation = require("../../config/validations.js");
const isEmpty = Validation.isEmpty
const mongoose = require("mongoose");
const { identity } = require("lodash");
var jwt = require('jsonwebtoken');
var config = require('../../config/connection.js');
var randtoken = require('rand-token').generator(); //for genrate random passwords

// New User register 
exports.register = (req, res) => {
  console.log('enter here for register');
  var response = {};
  var otp = randtoken.generate(6, "1234567890");
  const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
  if (req.body.phoneNo != '' && req.body.phoneNo.length <= 15 && emailRegexp.test(req.body.email)) {
    var user = new User();
    //find user if user already signup
    User.findOne({
      $or: [
        { phoneNo: req.body.phoneNo },
        { email: req.body.email }
      ]
    }).exec(function (err, Users) {
      console.log('users here>>>>', Users);
      if (err) {
        response.status = 0;
        response.message = 'Enter valid phone number';
        return res.send(response);
      } else {
        if (!Users) {
          // twillo for otp send
          const accountSid = 'ACb9fe571bd2bac2771654701e96e4635f';
          const authToken = '6521bc61d969c99cff8a3259f432cff5';
          const client = require('twilio')(accountSid, authToken);
          client.messages
            .create({
              to: '+917018714493',
              from: '+12544498571',
              body: 'Verify First: Your code is ' + otp,
            }).then(function (sendotp) {
              user.phoneNo = req.body.phoneNo;
              user.otp = otp;
              user.save((err, userData) => {
                if (err) {
                  response.status = 0;
                  response.message = err.message;
                  return res.send(response);
                } else {
                  response.status = 1;
                  response.message = 'OTP has been Sent';
                  response.otp = userData.otp;
                  response.user_id = userData._id;
                  return res.send(response);
                }
              })
            }).catch(function (error) {
              if (error.code === 21211) {
                response.status = 0;
                response.message = 'Not valid Phone No.';
                return res.send(response);
              }
            }).done();
        } else if (Users.phoneNo == req.body.phoneNo) {
          response.status = 409;
          response.message = 'Phone number already exist! Please login with another number';
          return res.send(response);
        } else if (Users.email == req.body.email) {
          response.status = 409;
          response.message = 'Email id already exist! Please login with another email id';
          return res.send(response);
        } else {
          response.status = 0;
          return res.send(response);
        }
      }
    })
  } else {
    response.status = 0;
    response.message = 'Please enter valid email id and phone number';
    return res.send(response);
  }
};

// User Otp Verify
exports.verifyOtp = (req, res) => {
  var data = req.body;
  var userId = mongoose.Types.ObjectId(data.user_id);
  var response = {};
  // Validations check for all fields
  if (isEmpty(data.user_id) || isEmpty(data.otp) || isEmpty(data.name) || isEmpty(data.email) || isEmpty(data.dialCode) || isEmpty(data.address) || isEmpty(data.password) || isEmpty(data.deviceToken) || isEmpty(data.deviceType)) {
    response.status = 0;
    response.message = 'Please fill all the fields';
    return res.send(response);
  } else {
    // Validations for check password
    var password = data.password;
    var regularExpression = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()+=-\?;,./{}|\":<>\[\]\\\' ~_]).{8,}/;
    if (regularExpression.test(password) == true) {
      User.findById({ '_id': userId }).exec(function (err, user) {
        if (err) {
          response.status = 0;
          response.message = err.message;
          return res.send(response);
        } else {
          if (Boolean(user)) {
            if (user.otp != data.otp) {
              response.status = 0;
              response.message = 'Otp you entered doesnot match';
              return res.send(response);
            } else {
              user.name = data.name;
              user.email = data.email;
              user.dialCode = data.dialCode;
              user.address = data.address;
              user.password = bcrypt.hashSync(data.password, 8);
              user.image = '';
              user.isPhoneVerified = true; // 0 for not not verify and 1 for verify
              user.deviceToken = data.deviceToken;
              user.deviceType = data.deviceType;
              // updating token
              var token = jwt.sign({ id: user._id }, config.secret, {
                expiresIn: 8640000 // expires in 24 hours
              });
              user.jwtToken = token;
              user.save((err, doc) => {
                if (err) {
                  response.status = 0;
                  response.message = 'Something went wrong';
                  return res.send(response);
                } else {
                  response.status = 1;
                  response.message = 'Phone number verified';
                  response.token = token;
                  return res.send(response);
                }
              });
            }
          } else {
            response.status = 0;
            response.message = 'User not found in our database';
            return res.send(response);
          }
        }
      })
    } else {
      response.status = 0;
      response.message = 'Password must consist eight characters including one uppercase letter, one lowercase letter, and one number or special character';
      return res.send(response);
    }
  }
};

// For change phone number
exports.changePhoneNo = (req, res) => {
  var data = req.body;
  var response = {};
  var otp = randtoken.generate(6, "1234567890");
  if (isEmpty(data.user_id) || isEmpty(data.phoneNo)) {
    response.status = 0;
    response.message = 'Please fill user id and phone Number';
    return res.send(response);
  } else {
    //find user if user already signup
    User.findOne({ '_id': mongoose.Types.ObjectId(data.user_id) }).exec(function (err, Users) {
      if (err) {
        response.status = 0;
        response.message = err.message;
        return res.send(response);
      } else {
        console.log('enter here with users value>>>', Users);
        if (Boolean(Users)) {
          // twillo for otp send
          const accountSid = 'ACb9fe571bd2bac2771654701e96e4635f';
          const authToken = '6521bc61d969c99cff8a3259f432cff5';
          const client = require('twilio')(accountSid, authToken);
          client.messages
            .create({
              to: '+917018714493',
              from: '+12544498571',
              body: 'Verify First: Your code is ' + otp,
            }).then(function (sendotp) {
              var newvalues = {
                $set: {
                  phoneNo: data.phoneNo,
                  otp: otp,
                }
              };
              var myquery = { '_id': mongoose.Types.ObjectId(data.user_id) };
              User.updateOne(myquery, newvalues, function (err, userData) {
                if (err) {
                  response.status = 0;
                  response.message = 'Please enter valid phone number';
                  return res.send(response);
                } else {
                  response.status = 1;
                  response.message = 'Otp has been send on your entered mobile number. Please check it for verify your account';
                  response.user_id = Users._id;
                  response.otp = otp;
                  return res.send(response);
                }
              })
            }).catch(function (error) {
              if (error.code === 21211) {
                response.status = 0;
                response.message = 'Not valid Phone No.';
                return res.send(response);
              }
            }).done();
        } else {
          response.status = 0;
          response.message = 'User not found';
          return res.send(response);
        }
      }
    })
  }
}
// User Login
exports.login = (req, res) => {
  var data = req.body;
  var response = {};
  if (isEmpty(data.password)) {
    response.status = 0;
    response.message = 'Please fill all the fields';
    return res.send(response);
  } else if (isEmpty(data.email) && isEmpty(data.phoneNo)) {
    response.status = 0;
    response.message = 'You have to fill valid phone number or email id';
    return res.send(response);
  } else {
    if (isEmpty(data.email)) {
      var where = { phoneNo: data.phoneNo };
    } else {
      var where = { email: data.email };
    }
    //find user if user already signup
    User.findOne(where, function (err, user) {
      if (err) {
        response.status = 0;
        response.message = 'Enter valid phone number';
        return res.send(response);
      } else {
        if (Boolean(user)) {
          if (user.isPhoneVerified == true) {
            var passwordIsValid = bcrypt.compareSync(data.password, user.password);
            if (!passwordIsValid) {
              response.status = 0;
              response.message = 'Password doesnot match. Please enter again';
              return res.send(response);
            } else {
              // updating token
              var token = jwt.sign({ id: user._id }, config.secret, {
                expiresIn: 8640000 // expires in 24 hours
              });
              var newvalues = { $set: { 'jwtToken': token } };
              User.updateOne(where, newvalues, function (err, doc) {
                if (err) {
                  response.status = 0;
                  response.message = 'Does not update token';
                  return res.send(response);
                } else {
                  if (user.isPerferenceSet == true) {
                    response.status = 1;
                    response.token = token;
                    response.message = 'User logged in';
                    return res.send(response);
                  } else {
                    response.status = 0;
                    response.message = 'Please select your preference first';
                    return res.send(response);
                  }
                }
              });
            }
          } else {
            response.status = 0;
            response.message = 'Please verfy your mobile no. first';
            return res.send(response);
          }
        } else {
          response.status = 0;
          response.message = 'User not found';
          return res.send(response);
        }
      }
    })
  }
}

// Select Perferences 
exports.selectPerference = (req, res) => {
  var data = req.body;
  var response = {};
  if (isEmpty(data.perference) || isEmpty(data.user_id)) {
    response.status = 0;
    response.message = 'Please fill all the fields';
    return res.send(response);
  } else {
    //find user if user already signup
    User.findOne({ '_id': mongoose.Types.ObjectId(data.user_id) }).exec(function (err, user) {
      if (err) {
        response.status = 0;
        response.message = err.message;
        return res.send(response);
      } else {
        if (Boolean(user)) {
          var newvalues = {
            $set: {
              isPerferenceSet: true,
              perference: data.perference,
            }
          };
          var myquery = { '_id': mongoose.Types.ObjectId(data.user_id) };
          User.updateOne(myquery, newvalues, function (err, userData) {
            if (err) {
              response.status = 0;
              response.message = 'Something went wrong';
              return res.send(response);
            } else {
              response.status = 1;
              response.message = 'Save perferences.';
              response.token = user.jwtToken;
              return res.send(response);
            }
          });
        } else {
          response.status = 204;
          response.message = 'User not found';
          return res.send(response);
        }
      }
    })
  }
}


const user = {}

user.forgotPassword = async (req, res) => {
  try {
    let data = req.body
    if(req.body.email==''){
      var search=req.body.phoneNo;
    }else{
      var search=req.body.email;
    }
    let isUser = await User.find(search)
    if (!isUser) {
      res.send({
        success: false,
        message: "error"
      })
    } else {
      let result = await User.findOneAndUpdate(
        { email: req.body.email },
        data,
        { new: true }
      )
      if (!result) {
        res.send({
          success: false,
          message: "cant update your password"
        })
      } else {
        res.send({
          success: true,
          message: "password updated successful", result
        })
      }
    }
  } catch (err) {
    res.send({
      success: false,
      message: err.message
    })
  }
}

user.search = async (req, res) => {
  console.log('enter here');
  try {
    let data = req.body
    let usersData = await User.find({ 'name': { '$regex': req.body.name, '$options': 'i' }},{friendAddedBy:req.body.friendAddedBy})
    if (!usersData)
      res.send({
        Success: false,
        message: "users not found "
      })
    else {
      let Data = await User.aggregate([
        {
          $project: {
            "name": 1, "location": 1
          }
        }
      ])
    }
  } catch (err) {
    res.send({
      Success: false,
      message: err.message
    })
  }
};

module.exports = user
