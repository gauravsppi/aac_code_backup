const mognoose = require('mongoose');
const express = require('express');
const FRIENDS = require("../../models/Users/friends.model");

const friends = {}

//add friend api
friends.addFriend = async (req, res) => {
    console.log('add here');
    try {
        let result = req.body
        result.friendAddedBy = req.userId;
        result.addedFriend = req.userId
        let isFriend = await new FRIENDS(result).save()
        if (isFriend)
            res.send({
                Success: true,
                message: "friend added succesful", isFriend
            })
        else {
            res.send({
                Success: false,
                message: "unable to add"
            })
        }
    } catch (err) {
        res.send({
            Success: false,
            message: err.message
        })
    }
};

//cancel friend request api
friends.cancelRequest = async (req, res) => {
    try {
        let criteria = ({ status: 'Pending' })
        let data = req.body
        let option = { new: true }
        let CancelReq = await FRIENDS.updateOne(criteria, data, option)
        if (CancelReq) {
            res.send({
                Success: true,
                message: "friend request canceled", CancelReq
            })
        }
        else {
            res.send({
                Success: false,
                message: "unavailable to cancel the request"
            })
        }
    }
    catch (err) {
        res.send({
            Success: false,
            message: err.message
        })
    }
};

//friend request list api
friends.pendingRequest = async (req, res) => {
    try {
        let data = req.body
        let PendingRequest = await FRIENDS.find(
            { friendAddedBy: req.body.friendAddedBy },
            { status:'Pending'},
            { addedFriend: req.body.addedFriend }
        )
        if (PendingRequest) {
            res.send({
                Success: true,
                message: "All pending request are here", PendingRequest
            })
        }
        else {
            res.send({
                Success: false,
                message="invalid request"
            })
        }
    } catch (err) {
        res.send({
            Success: false,
            message: err.message
        })
    }
};

// friend list api
friends.allFriends = async (req, res) => {
    try {
        let criteria = {
            $or:[
                {friendAddedBy:req.body.friendAddedBy},
                {addedFriend:req.body.addedFriend}
            ],
                status:'Accepted'
            
        }
        let AllFriends = await FRIENDS.find(criteria)
        if(!AllFriends){
            res.send({
                Success:false,
                message:"cant get the friendlist"
            })
        }else{
            res.send({
                Success:true,
                message:"your friend list", AllFriends
            })
        }
        
    }catch(err){
        res.send({
            Success:false,
            message:err.message
        })
    }
};

module.exports = friends;