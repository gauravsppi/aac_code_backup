// new validations rules
const Joi = require('joi');

exports.isEmpty = (value) => {
    return (value == null || value.length === 0 || value == "");
}
exports.addPetValidations=(data)=>
{
     JoiSchconstema = Joi.object({
        user_id: Joi.string().alphanum().required(),
        petName: Joi.string().required(),
        petType: Joi.string().required(),
        petColor: Joi.date().required(),
        petBread: Joi.string().required(),
        description: Joi.string().required(),
        images: Joi.array().required()
    }).unknown();
  
    return JoiSchema.validate(data);
}

//validation for friends controller
exports.friendSearchValidations=(data)=>
{
    const joiSchema = Joi.object({
        user_id: Joi.string().required(),
        name: Joi.string().required()
    }).unknown();
    return joiSchema.validate(data);
}