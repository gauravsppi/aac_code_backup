const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();
const _tokenManager = {};
// for data encrypt decryption crypto is used
const CryptoJS = require("crypto-js");

// check token
_tokenManager.authenticate = async (req, res, next) => {
    if (req.headers['x-token']) {
        let token = getToken(req);

        //verify if authenticated user.

        const secret = process.env.JWT_SECRET || "Development";
        jwt.verify(token, secret, async (err, decoded) => { // token verify

            if (decoded) {
                // if token verified then set req keys to middlewares
                req.userId = decoded.userId;
                req.email = decoded.email;
                req.role = decoded.role;
                next();
            } else {
                res.status(403).json({ // return for invalid token
                    success: false,
                    dateCheck: constant.dateCheck,
                    message: "Invalid token",
                });
            }
        });
    } else {

        res.status(403).json({ // return for invalid token
            success: false,
           
            message: "Token is not Provided ",
        });
    
    }
};

// get token from headers
const getToken = function (req) {
    if (
        req.headers &&
        req.headers['x-token'] &&
        req.headers['x-token'].split(" ")[0] === "Bearer"
    ) {
        return req.headers['x-token'].split(" ")[1];
    }
    return null;
};

module.exports = _tokenManager