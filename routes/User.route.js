const express = require('express');
const router = express.Router();
const AUTH = require('../middleware/auth')
const UsersController = require("../controllers/Users/LoginController.js");
const PetController = require("../controllers/Users/PetsController.js");
const FriendsController=require("../controllers/Users/FriendsController.js");
const friendsValidate = require(".././config/validations")


//  ONBOARDING API'S
router.post("/registerUser", UsersController.register);
router.post("/changePhoneNo", UsersController.changePhoneNo);
router.post("/verifyOtp", UsersController.verifyOtp);
router.post("/login", UsersController.login);
router.post("/savePerferences", UsersController.selectPerference);
router.post("/addPet", PetController.addPet);
router.post("/searchFriends",AUTH.authenticate, friendsValidate, UsersController.search);

// friends
router.group("/friends",(friends) => {
friends.post("/addFriends", AUTH.authenticate, FriendsController.addFriend);
friends.post("/cancelRequest",AUTH.authenticate,  FriendsController.cancelRequest);
friends.post("/pendingRequest",AUTH.authenticate,  FriendsController.pendingRequest);
friends.post("/allFriend", AUTH.authenticate, FriendsController.allFriends)
})

module.exports = router;
