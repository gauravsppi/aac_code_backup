const mongoose = require("mongoose");
const UserSchema = mongoose.Schema({
        email: {
            type: String
        },
        phoneNo: {
            type: Number
        },
        dialCode: {
            type: String
        },
        password: {
            type: String
        },
        name: {
            type: String
        },
        image: {
            type: String
        },
        address: {
            type: String
        },
        otp: {
            type: Number
        },
        isLike: {
            type: Boolean,
            default: true
        },
        isComment: {
            type: Boolean,
            default: true
        },
        isPerferenceSet:{
            type: Boolean,
            default: false
        },
        perference:{
            type: Array,
            default: ['']
        },
        location: {
            type: String
        },
        isProfileSetup: {
            type: Boolean,
            default: false
        },
        isPhoneVerified: {
            type: Boolean,
            default: false
        },
        isActive: {
            type: Boolean,
            default: false
        },
        isApproved: {
            type: Boolean,
            default: false
        },
        isBlocked: {
            type: Boolean,
            default: false
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
        jwtToken: {
            type: String
        },
        deviceToken: {
            type: String
        },
        deviceType: {
            type: String
        },
        isLike: {
            type: Boolean,
            default: true
        },
        isComment: {
            type: Boolean,
            default: true
        },
        isPostAccepted: {
            type: Boolean,
            default: true
        },
        isPostRejected: {
            type: Boolean,
            default: true
        },
    },{
        timestamps: true
});
module.exports = mongoose.model('User', UserSchema);