const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const friends = new Schema({
    status: {
        type: String,
        enum: ['Pending', 'Accepted', 'Rejected'],
        default: 'Pending'
    },
    friendAddedBy:{
        type: mongoose.Schema.Types.ObjectId, ref:'user'
    },
    addedFriend:{
        type: mongoose.Schema.Types.ObjectId, ref:'user'
    }
},{timestamps: true})

module.exports = mongoose.model('friends',friends)