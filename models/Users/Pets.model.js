const mongoose = require("mongoose");
const UserSchema = mongoose.Schema({
        user_id: {
            type: String
        },
        petName: {
            type: String
        },
        petType: {
            type: String
        },
        petColor: {
            type: String
        },
        petBread: {
            type: String
        },
        description: {
            type: String
        },
        petImages:{
            type: Array
        }
    },{
        timestamps: true
});
module.exports = mongoose.model('Pet', UserSchema);